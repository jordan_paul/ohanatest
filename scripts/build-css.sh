#! /bin/bash

mkdir -p web/sites/all/themes/salesforceohana/css

vendor/bin/pscss --style compressed --load_paths web/sites/all/themes/salesforceohana/scss < web/sites/all/themes/salesforceohana/scss/style.scss > web/sites/all/themes/salesforceohana/css/style.css
vendor/bin/pscss --style compressed --load_paths web/sites/all/themes/salesforceohana/scss < web/sites/all/themes/salesforceohana/scss/hiring-onboarding.scss > web/sites/all/themes/salesforceohana/css/hiring-onboarding.css
vendor/bin/pscss --style compressed --load_paths web/sites/all/themes/salesforceohana/scss < web/sites/all/themes/salesforceohana/scss/beacon-base.scss > web/sites/all/themes/salesforceohana/css/beacon-base.css
