#! /bin/bash

file="/app/.heroku/php/var/apache2/run/httpd.pid"

if [ -f $file ] ; then
    echo "Removing old httpd pid file"
    rm $file
fi
