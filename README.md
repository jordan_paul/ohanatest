
# Salesforce Ohana

* [Prepping Local Dev via Docker](#prepping-dev-via-docker)
* [SimpleSamlPHP](#all-about-simplesaml)
* [Working on a Theme](#theme-work)

## Development Instructions

Note this documentation is written for OS X.

### Dependencies

* [Docker](https://www.docker.com/community-edition)
* [Composer](https://getcomposer.org/)
* Administrative access should be granted for the staging and production sites.

### Set Up

#### Clone Repository

First clone the repository from github to a local project folder:

```
$ git clone git@github.com:SalesforceCloudServices/salesforce-ohana.git
```

#### Install Dependencies

Composer is used to manage dependencies. There are various scripts and third party binaries required to build the application successfully (SASS compilation for example). Run the following command from the root of the project to install these dependencies.

```
$ composer install
```

#### Create .env file

When running locally, heroku uses a .env file to emulate the environment variables that you can set in the heroku dashborad.

This file should have a key, value based environment variables, one to each line. This takes the format of:

```
variable1=value1
variable2=value2
```

See the heroku configuration on the dev environment for a good set of env variables for development/local. Or ask one of the developers for a copy of their .env file.

These environment variables should be properly set:

* DATABASE_URL
* BASE_URL
* ENV=local
* BUCKETEER_AWS_ACCESS_KEY_ID
* BUCKETEER_AWS_SECRET_ACCESS_KEY
* BUCKETEER_BUCKET_NAME


#### User Docker-Compose to run both the application and the database

Run the following command from the root of the project to spin up the application.

```
$ docker-compose up
```

This command:

1. Builds both the ohana-db (postgres) image and the ohana-server application image.
2. Runs the docker container for the application, in the background,  by setting port 80 of the container to port 8080 on the local machine
3. links the current working directory (your sourcecode) to the /var/www/html directory inside the container.

#### Restore Copy of Database Locally

1. Download a database backup from Heroku (there is no file extension).
2. SSH into the Docker container with the above instructions.
3. Run the following command from the root of your project directory:

  ```
  pg_restore --verbose --clean --no-acl --no-owner -h localhost -U postgres -d ohana-db <filename>
  ```

#### Access the site

** Finally, head to https://localhost:8443 **

Note: SSO Login only works on HTTPS.

### Accessing the ohana-server contianer while it's running

You may need to ssh into the running ohana-server container. This is helpful, because it allows you to

1. Run drush
2. See the file structure/system as it appears while the server is running
3. Access SimpleSamlPHP logs.

To access the running container via docker run the following command:

```
$ docker exec -it ohana-server bash
```

### Things are Fudged

When all seems broken, and you can not access the site at all, a registry rebuild may be in order. We're using a [module](https://www.drupal.org/project/registry_rebuild) to help us with that, so you can access the tool at `/sites/all/modules/registry_rebuild/registry_rebuild.php` and let it do its magic. This is a password protected url.

### All About SimpleSaml

Add information on SimpleSamlPHP here. Some helpful links currently:

* [Installing SimpleSaml Independantly](https://simplesamlphp.org/docs/stable/simplesamlphp-install)

### Theme Work

Most of the work on this blog will occur in the `web/sites/all/themes/salesforceohana` directory.

To compile styles, change to the `salesforceohana` directory, and run `compass compile`. The scss files will compile into `css/style.css`, which is included in this theme's .info file. Add your styles to the `scss` directory in the child theme. You can also `compass watch` to let the file get compiled as you work.

_TODO: when this project gets pushed to development and production environments, how do we get this style file to get compiled? Should we just include it in version control?_
