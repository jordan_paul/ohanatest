<?php
/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote
 */
if($_ENV["ENV"] === 'local'){
    //Use local auth source metadata!
    $metadata['https://abnovak-dev-ed.my.salesforce.com'] = array (
        'entityid' => 'https://abnovak-dev-ed.my.salesforce.com',
        'contacts' =>
            array (
            ),
        'metadata-set' => 'saml20-idp-remote',
        'expire' => 1797726447,
        'SingleSignOnService' =>
            array (
                0 =>
                    array (
                        'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                        'Location' => 'https://abnovak-dev-ed.my.salesforce.com/idp/endpoint/HttpPost',
                    ),
                1 =>
                    array (
                        'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                        'Location' => 'https://abnovak-dev-ed.my.salesforce.com/idp/endpoint/HttpRedirect',
                    ),
            ),
        'SingleLogoutService' =>
            array (
            ),
        'ArtifactResolutionService' =>
            array (
            ),
        'NameIDFormats' =>
            array (
                0 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
            ),
        'keys' =>
            array (
                0 =>
                    array (
                        'encryption' => false,
                        'signing' => true,
                        'type' => 'X509Certificate',
                        'X509Certificate' => 'MIIErDCCA5SgAwIBAgIOAVbYH6yqAAAAAAdzwsQwDQYJKoZIhvcNAQELBQAwgZAxKDAmBgNVBAMMH1NlbGZTaWduZWRDZXJ0XzI5QXVnMjAxNl8yMTA2MDgxGDAWBgNVBAsMDzAwRDM2MDAwMDAwWVR6NzEXMBUGA1UECgwOU2FsZXNmb3JjZS5jb20xFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xCzAJBgNVBAgMAkNBMQwwCgYDVQQGEwNVU0EwHhcNMTYwODI5MjEwNjA5WhcNMTcwODI5MTIwMDAwWjCBkDEoMCYGA1UEAwwfU2VsZlNpZ25lZENlcnRfMjlBdWcyMDE2XzIxMDYwODEYMBYGA1UECwwPMDBEMzYwMDAwMDBZVHo3MRcwFQYDVQQKDA5TYWxlc2ZvcmNlLmNvbTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzELMAkGA1UECAwCQ0ExDDAKBgNVBAYTA1VTQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKjlMnyqxUPM+AFjdRN9IihXQ90+6GSHRPyGXd7xtEZwipbroQvF2o1tPn7AfPg9XXR9tzRX8HelCufs3ce61bjrlvDLwyIiFiAKM7cy4bv5ZMITLQGRTSeb4ZZEicandhPOWNKcQPdFySfarUYJHJkkLjZcOE6JHGe/h4BE8YAuZsqHIvAuiLvbyr8se2pFw5vxsZtMnqwjVEQYpi22CRPAFImz/urn52S2yleSVlfzegFnzQpm/iSxwbscX4vEZYwNZ/qA+ojsPlrR0btNL9gKhpUm8DRZLhpE7I+z5T3AdEoJdoHzgPgsP2mfkqm3fJXIIPuQwHCftQbq/c6CLDsCAwEAAaOCAQAwgf0wHQYDVR0OBBYEFFZmGocF9d3yK5JzeIUYgJftNBcXMA8GA1UdEwEB/wQFMAMBAf8wgcoGA1UdIwSBwjCBv4AUVmYahwX13fIrknN4hRiAl+00FxehgZakgZMwgZAxKDAmBgNVBAMMH1NlbGZTaWduZWRDZXJ0XzI5QXVnMjAxNl8yMTA2MDgxGDAWBgNVBAsMDzAwRDM2MDAwMDAwWVR6NzEXMBUGA1UECgwOU2FsZXNmb3JjZS5jb20xFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xCzAJBgNVBAgMAkNBMQwwCgYDVQQGEwNVU0GCDgFW2B+sqgAAAAAHc8LEMA0GCSqGSIb3DQEBCwUAA4IBAQCa1sExQjbKGrBH9BRMbPfZcnPzCwEvsNeYn+vZh+pWZqhNJVCxk3L9nq7LgdVC/Q2LJqz7OMGTQcF8+Q90OikDWGWPI9u1MQqPuMXnaU9RZXr/OYvn96HIXSM1rQ/f7INhXOQBXV1jtxAaDK7ixcV18QlQkHZVNLI//q1V+eTY2n6UVu0iCVrouVAfEuazSb5qg/QYNa6J6mYHFTOKLfUUkYYIZtXAMAy+4awAk2DZ4HDBqPNdtrBOi5C1pm28JZlM1UR0skDcXoPWZVLUspUZoD74v2Y4luoYL0xbh+XiKufaT53JNn/VXRl3CPmJOr/Y0T8/yEfdZsqMFnLLgaHZ',
                    ),
            ),
    );
} else{
    //Use Dev metadata!!
    $metadata['https://abnovak-dev-ed.my.salesforce.com'] = array (
        'entityid' => 'https://abnovak-dev-ed.my.salesforce.com',
        'contacts' =>
            array (
            ),
        'metadata-set' => 'saml20-idp-remote',
        'expire' => 1799598150,
        'SingleSignOnService' =>
            array (
                0 =>
                    array (
                        'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                        'Location' => 'https://abnovak-dev-ed.my.salesforce.com/idp/endpoint/HttpPost',
                    ),
                1 =>
                    array (
                        'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                        'Location' => 'https://abnovak-dev-ed.my.salesforce.com/idp/endpoint/HttpRedirect',
                    ),
            ),
        'SingleLogoutService' =>
            array (
            ),
        'ArtifactResolutionService' =>
            array (
            ),
        'NameIDFormats' =>
            array (
                0 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
            ),
        'keys' =>
            array (
                0 =>
                    array (
                        'encryption' => false,
                        'signing' => true,
                        'type' => 'X509Certificate',
                        'X509Certificate' => 'MIIErDCCA5SgAwIBAgIOAVbYH6yqAAAAAAdzwsQwDQYJKoZIhvcNAQELBQAwgZAxKDAmBgNVBAMMH1NlbGZTaWduZWRDZXJ0XzI5QXVnMjAxNl8yMTA2MDgxGDAWBgNVBAsMDzAwRDM2MDAwMDAwWVR6NzEXMBUGA1UECgwOU2FsZXNmb3JjZS5jb20xFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xCzAJBgNVBAgMAkNBMQwwCgYDVQQGEwNVU0EwHhcNMTYwODI5MjEwNjA5WhcNMTcwODI5MTIwMDAwWjCBkDEoMCYGA1UEAwwfU2VsZlNpZ25lZENlcnRfMjlBdWcyMDE2XzIxMDYwODEYMBYGA1UECwwPMDBEMzYwMDAwMDBZVHo3MRcwFQYDVQQKDA5TYWxlc2ZvcmNlLmNvbTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzELMAkGA1UECAwCQ0ExDDAKBgNVBAYTA1VTQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKjlMnyqxUPM+AFjdRN9IihXQ90+6GSHRPyGXd7xtEZwipbroQvF2o1tPn7AfPg9XXR9tzRX8HelCufs3ce61bjrlvDLwyIiFiAKM7cy4bv5ZMITLQGRTSeb4ZZEicandhPOWNKcQPdFySfarUYJHJkkLjZcOE6JHGe/h4BE8YAuZsqHIvAuiLvbyr8se2pFw5vxsZtMnqwjVEQYpi22CRPAFImz/urn52S2yleSVlfzegFnzQpm/iSxwbscX4vEZYwNZ/qA+ojsPlrR0btNL9gKhpUm8DRZLhpE7I+z5T3AdEoJdoHzgPgsP2mfkqm3fJXIIPuQwHCftQbq/c6CLDsCAwEAAaOCAQAwgf0wHQYDVR0OBBYEFFZmGocF9d3yK5JzeIUYgJftNBcXMA8GA1UdEwEB/wQFMAMBAf8wgcoGA1UdIwSBwjCBv4AUVmYahwX13fIrknN4hRiAl+00FxehgZakgZMwgZAxKDAmBgNVBAMMH1NlbGZTaWduZWRDZXJ0XzI5QXVnMjAxNl8yMTA2MDgxGDAWBgNVBAsMDzAwRDM2MDAwMDAwWVR6NzEXMBUGA1UECgwOU2FsZXNmb3JjZS5jb20xFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xCzAJBgNVBAgMAkNBMQwwCgYDVQQGEwNVU0GCDgFW2B+sqgAAAAAHc8LEMA0GCSqGSIb3DQEBCwUAA4IBAQCa1sExQjbKGrBH9BRMbPfZcnPzCwEvsNeYn+vZh+pWZqhNJVCxk3L9nq7LgdVC/Q2LJqz7OMGTQcF8+Q90OikDWGWPI9u1MQqPuMXnaU9RZXr/OYvn96HIXSM1rQ/f7INhXOQBXV1jtxAaDK7ixcV18QlQkHZVNLI//q1V+eTY2n6UVu0iCVrouVAfEuazSb5qg/QYNa6J6mYHFTOKLfUUkYYIZtXAMAy+4awAk2DZ4HDBqPNdtrBOi5C1pm28JZlM1UR0skDcXoPWZVLUspUZoD74v2Y4luoYL0xbh+XiKufaT53JNn/VXRl3CPmJOr/Y0T8/yEfdZsqMFnLLgaHZ',
                    ),
            ),
    );
}

$metadata['https://aloha.my.salesforce.com'] = array (
    'entityid' => 'https://aloha.my.salesforce.com',
    'contacts' =>
        array (
        ),
    'metadata-set' => 'saml20-idp-remote',
    'expire' => 1754935712,
    'SingleSignOnService' =>
        array (
            0 =>
                array (
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
                    'Location' => 'https://aloha.my.salesforce.com/idp/endpoint/HttpPost',
                ),
            1 =>
                array (
                    'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
                    'Location' => 'https://aloha.my.salesforce.com/idp/endpoint/HttpRedirect',
                ),
        ),
    'SingleLogoutService' =>
        array (
        ),
    'ArtifactResolutionService' =>
        array (
        ),
    'NameIDFormats' =>
        array (
            0 => 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified',
        ),
    'keys' =>
        array (
            0 =>
                array (
                    'encryption' => false,
                    'signing' => true,
                    'type' => 'X509Certificate',
                    'X509Certificate' => 'MIIGwTCCBamgAwIBAgITOwAAK9VyNDAvUES+OAACAAAr1TANBgkqhkiG9w0BAQsFADBjMRMwEQYKCZImiZPyLGQBGRYDY29tMRowGAYKCZImiZPyLGQBGRYKc2FsZXNmb3JjZTEYMBYGCgmSJomT8ixkARkWCGludGVybmFsMRYwFAYDVQQDEw1BU0gtQ0EtV1AxLUNBMB4XDTE3MDgwNzE5NTUwMVoXDTIwMDgwNjE5NTUwMVowgb4xCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpDYWxpZm9ybmlhMRYwFAYDVQQHEw1TYW4gRnJhbmNpc2NvMRcwFQYDVQQKEw5TYWxlc2ZvcmNlLmNvbTEdMBsGA1UECxMUSVQgU2VjdXJpdHkgU2VydmljZXMxIDAeBgNVBAMTF2Fsb2hhLm15LnNhbGVzZm9yY2UuY29tMSgwJgYJKoZIhvcNAQkBFhlpdHNzLWFsb2hhQHNhbGVzZm9yY2UuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAszYrgXAZuppe+RHuw0faltKh4VxPYiRqe45XUM3aObj9NCL+EavlKV9azyte8y0wpFEuuQO7ALpHTxrMk3rCnR+D5FuE3DFSFnbIJjUQOQ+9JSAtHiFzKLMSZVtYhsDu6LU8GRx07zFRoGOodQfz8/YMB1e3wfwtWnsWe7Wz6LXa47Ui0uNGCbQTycIF+TDIRtt9Z/CrS9/tc2Up3mDbv+deNIAdi2ZlFhi3dgrK2H+QFKs2GA3QsdI6XlrkskcNR2p17GWL7yXSYrTsrZN1mlCgHZU/zlNu+qn90roZBlQ/nfzW9bm7GfLe1Oh39rsn6S/vOwIOqMU99IdIe62gvQIDAQABo4IDEDCCAwwwHQYDVR0OBBYEFHKH9ZV0M1Un84DwNzfpKJ68iLZ2MB8GA1UdIwQYMBaAFH72wRvUQLG+0WXt6tNi06oZ2PniMIIBKQYDVR0fBIIBIDCCARwwggEYoIIBFKCCARCGQmh0dHA6Ly9jcmwuaW50ZXJuYWwuc2FsZXNmb3JjZS5jb20vYXNoLWNhLXdwMS9BU0gtQ0EtV1AxLUNBKDIpLmNybIaByWxkYXA6Ly8vQ049QVNILUNBLVdQMS1DQSgyKSxDTj1hc2gtY2Etd3AxLENOPUNEUCxDTj1QdWJsaWMlMjBLZXklMjBTZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPWludGVybmFsLERDPXNhbGVzZm9yY2UsREM9Y29tP2NlcnRpZmljYXRlUmV2b2NhdGlvbkxpc3Q/YmFzZT9vYmplY3RDbGFzcz1jUkxEaXN0cmlidXRpb25Qb2ludDCCAQcGCCsGAQUFBwEBBIH6MIH3MIG7BggrBgEFBQcwAoaBrmxkYXA6Ly8vQ049QVNILUNBLVdQMS1DQSxDTj1BSUEsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlvbixEQz1pbnRlcm5hbCxEQz1zYWxlc2ZvcmNlLERDPWNvbT9jQUNlcnRpZmljYXRlP2Jhc2U/b2JqZWN0Q2xhc3M9Y2VydGlmaWNhdGlvbkF1dGhvcml0eTA3BggrBgEFBQcwAYYraHR0cDovL29jc3Bhc2guaW50ZXJuYWwuc2FsZXNmb3JjZS5jb20vb2NzcDALBgNVHQ8EBAMCBaAwPQYJKwYBBAGCNxUHBDAwLgYmKwYBBAGCNxUIh/S7SIfX1QuE+ZMUgZ7cY4TOvi6BU/vWaob9olECAWQCAREwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMBMCcGCSsGAQQBgjcVCgQaMBgwCgYIKwYBBQUHAwIwCgYIKwYBBQUHAwEwDQYJKoZIhvcNAQELBQADggEBADSHP1FCmjqqbObPqSp27eby/Fj3V8KWN+pIFZ5GgUi3hQMGm6kGEbEitXxFXj0rUVVm5iQhz6yrX1xDeklJh+sY9pG1eUsvjgMhz8V8EGQfaAcLyW5G4+wEp7UTXch/IWloI0CDUejDc7lAuz7kjJ4/7biJ+sjEFaFzbnyBj/j0Gywuwz0maA0dsc7usEQq/ytqZX3Nq81RE3rHX4MSxIWYEHbQgao2Fhum5t7ZjD3lMlPPhrtQvBIZfwObHk/80Iqa5EGK+XLz9mBoPK1/195pmXkiybDxV/v+Jk4S3PyWBpVWsswa0kuKBQM9FZ6AN61kYPvMaZRJcn+1JIb8P8A=',
                ),
        ),
);
