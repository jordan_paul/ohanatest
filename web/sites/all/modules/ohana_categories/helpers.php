<?php

/*
 * Helper functions for install, enable, disable, and uninstall hooks
 * of the Ohana Categories Module
 */

function add_vocab_and_terms_and_menu() {
  $vocab_results = add_vocab();
  $term_results = add_terms();
  $menu_additions = add_menu_items();

  return [
    'term_message' => $vocab_results . ' ' . $term_results,
    'menu_message' => $menu_additions
  ];
}

function add_vocab() {
  // does the vocabulary exist?
  $existing_vocabulary = taxonomy_vocabulary_machine_name_load('ohana_categories');
  if (!$existing_vocabulary) {
    // add the containing vocabulary
    $new_vocabulary = (object) array(
      'name'         => 'Ohana Categories',
      'description'  => 'terms to use to organise posts',
      'machine_name' => 'ohana_categories'
    );

    taxonomy_vocabulary_save($new_vocabulary);
    return 'A new vocabulary, Ohana Categories, has been added.';
  } else {
    return 'The vocabulary Ohana Categories is still available.';
  }
}

function add_terms() {
  $vocab = taxonomy_vocabulary_machine_name_load('ohana_categories');
  $vid = $vocab->vid;

  $terms = [];
  $terms_added = 0;

  $terms[] = (object) [ 'name' => 'Trust', 'vid' => $vid ];
  $terms[] = (object) [ 'name' => 'Customer Success', 'vid' => $vid ];
  $terms[] = (object) [ 'name' => 'Innovation', 'vid' => $vid ];
  $terms[] = (object) [ 'name' => 'Giving Back', 'vid' => $vid ];
  $terms[] = (object) [ 'name' => 'Equality', 'vid' => $vid ];
  $terms[] = (object) [ 'name' => 'Wellness', 'vid' => $vid ];
  $terms[] = (object) [ 'name' => 'Transparency', 'vid' => $vid ];
  $terms[] = (object) [ 'name' => 'Fun', 'vid' => $vid ];

  foreach ($terms as $term) {
    $exists = taxonomy_get_term_by_name($term->name, 'ohana_categories');

    if (empty($exists)) {
      taxonomy_term_save($term);
      $terms_added++;

      alter_term_alias($term->name, $term->tid);
    }
  }

 return $terms_added . ' terms(s) have been added.';
}

function alter_term_alias($term_name, $term_id) {
  // http://lechronicles.blogspot.com/2014/01/alter-path-alias-programmatically-for.html
  $term = taxonomy_term_load($term_id);
  $alias = strtolower($term_name);
  $alias = str_replace(' ', '-', $alias);

  $term->path['pathauto'] = 0;
  $term->path['alias'] = $alias;
  taxonomy_term_save($term);
}

function add_menu_items() {
  $menu_items = [ 'Trust', 'Customer Success', 'Innovation',
    'Giving Back', 'Equality', 'Wellness', 'Transparency', 'Fun' ];
  $added_menu_item_count = 0;

  foreach ($menu_items as $key => $item) {
    if (!menu_item_exists($item)) {
      $alias = strtolower($item);
      $alias = str_replace(' ', '-', $alias);
      $link_path = drupal_get_normal_path($alias);

      $menu_item = [
        'link_title' => t($item),
        'menu_name'  => 'main-menu',
        'link_path'  => $link_path,
        'weight'     => $key + 1
      ];

      menu_link_save($menu_item);
      $added_menu_item_count++;
    }
  }

  if ($added_menu_item_count) {
    menu_cache_clear_all();
    return $added_menu_item_count . ' link(s) have been added to the Main Menu.';
  } else {
    return '';
  }
}

function menu_item_exists($item_title) {
  $main_menu = menu_load_links($menu_name = 'main-menu');
  $found_match = false;

  foreach ($main_menu as $menu_item) {
    if ($menu_item['link_title'] === $item_title) {
      $found_match = true;
    }
  }

  return $found_match;
}

function remove_vocab() {
  $vocab = taxonomy_vocabulary_machine_name_load('ohana_categories');
  taxonomy_vocabulary_delete($vocab->vid);
}

function remove_menu_items() {
  $main_menu = menu_load_links($menu_name = 'main-menu');
  $titles = [ 'Trust', 'Customer Success', 'Innovation',
    'Giving Back', 'Equality', 'Wellness', 'Transparency', 'Fun' ];

  foreach($main_menu as $menu_item) {
    if (in_array($menu_item['link_title'], $titles)) {
      menu_link_delete($mlid = $menu_item['mlid']);
    }
  }
}
