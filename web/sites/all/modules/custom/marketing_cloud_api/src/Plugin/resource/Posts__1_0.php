<?php

/**
 * @file
 * Contains \Drupal\restful_example\Plugin\resource\Posts__1_0.
 */

namespace Drupal\marketing_cloud_api\Plugin\resource;

use Drupal\restful\Plugin\resource\DataInterpreter\DataInterpreterInterface;
use Drupal\restful\Plugin\resource\Field\ResourceFieldBase;
use Drupal\restful\Plugin\resource\Field\ResourceFieldInterface;
use Drupal\restful\Plugin\resource\ResourceEntity;
use Drupal\restful\Plugin\resource\ResourceInterface;
use Drupal\restful\Plugin\resource\ResourceNode;

/**
 * Class Tags
 * @package Drupal\restful\Plugin\resource
 *
 * @Resource(
 *   name = "posts:1.0",
 *   resource = "posts",
 *   label = "Posts",
 *   description = "List ohana blog posts.",
 *   authenticationOptional = FALSE,
 *   authenticationTypes = {
 *     "basic_auth"
 *   },
 *   dataProvider = {
 *     "entityType": "node",
 *     "bundles": {
 *       "blog_post"
 *     }
 *   },
 *   majorVersion = 1,
 *   minorVersion = 0
 * )
 */
class Posts__1_0 extends ResourceEntity implements ResourceInterface {
  /**
   * {@inheritdoc}
   */
  protected function publicFields() {
    $public_fields = parent::publicFields();

    $public_fields['created'] = array(
      'property' => 'created'
    );

    $public_fields['title'] = array(
      'property' => 'title'
    );

    $public_fields['body'] = array(
      'property' => 'body',
      'sub_property' => 'summary'
    );

    $public_fields['category'] = array(
      'property' => 'field_blog_category',
      'process_callbacks' => array(
        array($this, 'categoryProcess')
       )
    );

    $public_fields['tags'] = array(
      'property' => 'field_blog_tagging',
      'process_callbacks' => array(
        array($this, 'tagProcess')
       )
    );

    $public_fields['author'] = array(
      'property' => 'field_blog_author',
      'sub_property' => 'title'
    );

    $public_fields['thumbnail'] = array(
      'property' => 'field_blog_square_image'
    );

    return $public_fields;
  }

  public function tagProcess($value) {
    if (is_array($value)) {
      $output = array();
      foreach ($value as $item) {
        $output[] = $this->tagProcess($item);
      }
      return $output;
    }

    $tag_obj = taxonomy_term_load($value);
    if ($tag_obj) {
     return $tag_obj->name;
    } else {
     return false;
    }
  }

  public function categoryProcess($value) {
    if (!empty($value)) {
      $cat_obj = taxonomy_term_load($value);
      if ($cat_obj) {
       return $cat_obj->name;
      } else {
       return false;
      }
    }

    return null;
  }
}
