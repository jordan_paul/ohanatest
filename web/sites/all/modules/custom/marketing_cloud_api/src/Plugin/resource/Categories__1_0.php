<?php

/**
 * @file
 * Contains \Drupal\restful_example\Plugin\resource\Categories__1_0.
 */

namespace Drupal\marketing_cloud_api\Plugin\resource;

use Drupal\restful\Plugin\resource\DataInterpreter\DataInterpreterInterface;
use Drupal\restful\Plugin\resource\Field\ResourceFieldInterface;
use Drupal\restful\Plugin\resource\ResourceEntity;
use Drupal\restful\Plugin\resource\ResourceInterface;

/**
 * Class Tags
 * @package Drupal\restful\Plugin\resource
 *
 * @Resource(
 *   name = "categories:1.0",
 *   resource = "categories",
 *   label = "Categories",
 *   description = "Export the ohana category taxonomy terms.",
 *   authenticationOptional = FALSE,
 *   authenticationTypes = {
 *     "basic_auth"
 *   },
 *   dataProvider = {
 *     "entityType": "taxonomy_term",
 *     "bundles": {
 *       "ohana_categories"
 *     },
 *   },
 *   majorVersion = 1,
 *   minorVersion = 0
 * )
 */
class Categories__1_0 extends ResourceEntity implements ResourceInterface {

}
