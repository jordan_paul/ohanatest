<?php
/**
 * @file
 * ohana_blog_post_content_type.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ohana_blog_post_content_type_taxonomy_default_vocabularies() {
  return array(
    'blog_tags' => array(
      'name' => 'Blog Tags',
      'machine_name' => 'blog_tags',
      'description' => 'add tags to blog posts for even finer organisation',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'ohana_categories' => array(
      'name' => 'Ohana Categories',
      'machine_name' => 'ohana_categories',
      'description' => 'terms to use to organise posts',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
