<?php
/**
 * @file
 * ohana_blog_post_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ohana_blog_post_content_type_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ohana_blog_post_content_type_node_info() {
  $items = array(
    'blog_post' => array(
      'name' => t('Blog Post'),
      'base' => 'node_content',
      'description' => t('Add posts about the happenings at Salesforce '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('A content publisher must accept the content of this post before it gets published. Please notify your contact that the content is ready for review.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
