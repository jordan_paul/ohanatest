<?php
/**
 * @file
 * ohana_blog_post_content_type.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ohana_blog_post_content_type_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_content|node|blog_post|form';
  $field_group->group_name = 'group_blog_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog_post';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Blog Main Content',
    'weight' => '3',
    'children' => array(
      0 => 'body',
      1 => 'field_body_paragraph_bottom',
      2 => 'group_rollup_sections',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-blog-content field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_blog_content|node|blog_post|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_ending|node|blog_post|form';
  $field_group->group_name = 'group_blog_ending';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog_post';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Blog Post Ending',
    'weight' => '5',
    'children' => array(
      0 => 'field_blog_ending_remarks',
      1 => 'field_blog_ending_remarks_cta',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-blog-ending field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_blog_ending|node|blog_post|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_image_grid_tab|node|blog_post|form';
  $field_group->group_name = 'group_blog_image_grid_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog_post';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_blog_image_tab_container';
  $field_group->data = array(
    'label' => 'Image Grid',
    'weight' => '5',
    'children' => array(
      0 => 'field_blog_image_grid',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-blog-image-grid-tab field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_blog_image_grid_tab|node|blog_post|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_image_tab_container|node|blog_post|form';
  $field_group->group_name = 'group_blog_image_tab_container';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog_post';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_post_images';
  $field_group->data = array(
    'label' => 'Blog Post Images',
    'weight' => '5',
    'children' => array(
      0 => 'group_blog_image_grid_tab',
      1 => 'group_blog_video_information_tab',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-blog-image-tab-container field-group-htabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_blog_image_tab_container|node|blog_post|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_video_information_tab|node|blog_post|form';
  $field_group->group_name = 'group_blog_video_information_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog_post';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_blog_image_tab_container';
  $field_group->data = array(
    'label' => 'Video Information',
    'weight' => '6',
    'children' => array(
      0 => 'field_blog_video_embed',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Video Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-blog-video-information-tab field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_blog_video_information_tab|node|blog_post|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_post_images|node|blog_post|form';
  $field_group->group_name = 'group_post_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog_post';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'blog post images',
    'weight' => '2',
    'children' => array(
      0 => 'field_blog_square_image',
      1 => 'field_blog_featured_image',
      2 => 'group_blog_image_tab_container',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-post-images field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_post_images|node|blog_post|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_post_information|node|blog_post|form';
  $field_group->group_name = 'group_post_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog_post';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'blog post information',
    'weight' => '1',
    'children' => array(
      0 => 'field_blog_external_link',
      1 => 'field_exclude_from_main_blog',
      2 => 'field_blog_is_top_post',
      3 => 'field_blog_tagging',
      4 => 'field_blog_add_to_more_stories',
      5 => 'field_blog_type',
      6 => 'field_blog_category',
      7 => 'field_blog_author',
      8 => 'field_blog_internal_chatter_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-post-information field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_post_information|node|blog_post|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_rollup_sections|node|blog_post|form';
  $field_group->group_name = 'group_rollup_sections';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog_post';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_blog_content';
  $field_group->data = array(
    'label' => 'Blog Rollup Sections',
    'weight' => '6',
    'children' => array(
      0 => 'field_category_example',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-rollup-sections field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_rollup_sections|node|blog_post|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blog Main Content');
  t('Blog Post Ending');
  t('Blog Post Images');
  t('Blog Rollup Sections');
  t('Image Grid');
  t('Video Information');
  t('blog post images');
  t('blog post information');

  return $field_groups;
}
