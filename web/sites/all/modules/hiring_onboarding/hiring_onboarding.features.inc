<?php
/**
 * @file
 * hiring_onboarding.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hiring_onboarding_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function hiring_onboarding_node_info() {
  $items = array(
    'hiring' => array(
      'name' => t('Hiring'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'onboarding' => array(
      'name' => t('Onboarding'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
