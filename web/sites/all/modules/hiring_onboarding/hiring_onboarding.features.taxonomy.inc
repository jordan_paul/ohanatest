<?php
/**
 * @file
 * hiring_onboarding.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function hiring_onboarding_taxonomy_default_vocabularies() {
  return array(
    'onboarding_hiring_content_type' => array(
      'name' => 'Onboarding & Hiring Content Type',
      'machine_name' => 'onboarding_hiring_content_type',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
