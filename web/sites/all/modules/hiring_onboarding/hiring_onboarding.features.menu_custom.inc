<?php
/**
 * @file
 * hiring_onboarding.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function hiring_onboarding_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-hiring.
  $menus['menu-hiring'] = array(
    'menu_name' => 'menu-hiring',
    'title' => 'Hiring',
    'description' => 'Navigation Menu for the hiring section of the site.',
  );
  // Exported menu: menu-onboarding.
  $menus['menu-onboarding'] = array(
    'menu_name' => 'menu-onboarding',
    'title' => 'Onboarding',
    'description' => 'Navigation Menu for the onboarding section of the site.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Hiring');
  t('Navigation Menu for the hiring section of the site.');
  t('Navigation Menu for the onboarding section of the site.');
  t('Onboarding');

  return $menus;
}
