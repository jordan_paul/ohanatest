<?php
/**
 * @file
 * the_post_features.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function the_post_features_taxonomy_default_vocabularies() {
  return array(
    'post_categories' => array(
      'name' => 'Post Categories',
      'machine_name' => 'post_categories',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'post_general' => array(
      'name' => 'Post General',
      'machine_name' => 'post_general',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'post_industries' => array(
      'name' => 'Post Industries',
      'machine_name' => 'post_industries',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
