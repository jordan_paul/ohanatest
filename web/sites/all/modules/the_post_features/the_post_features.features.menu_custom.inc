<?php
/**
 * @file
 * the_post_features.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function the_post_features_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-beacon-industry-menu.
  $menus['menu-beacon-industry-menu'] = array(
    'menu_name' => 'menu-beacon-industry-menu',
    'title' => 'Post Industry Menu',
    'description' => '',
  );
  // Exported menu: menu-beacon-menu-top.
  $menus['menu-beacon-menu-top'] = array(
    'menu_name' => 'menu-beacon-menu-top',
    'title' => 'Post Menu Top',
    'description' => '',
  );
  // Exported menu: menu-mixed-blog-menu.
  $menus['menu-mixed-blog-menu'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'title' => 'mixed-blog-menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Post Industry Menu');
  t('Post Menu Top');
  t('mixed-blog-menu');

  return $menus;
}
