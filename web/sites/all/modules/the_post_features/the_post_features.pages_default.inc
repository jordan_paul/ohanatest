<?php
/**
 * @file
 * the_post_features.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function the_post_features_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'astro_categories';
  $page->task = 'page';
  $page->admin_title = 'Beacon Categories';
  $page->admin_description = '';
  $page->path = 'beacon-cat';
  $page->access = array(
    'logic' => 'and',
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
  );
  $page->menu = array(
    'type' => 'none',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_astro_categories__template';
  $handler->task = 'page';
  $handler->subtask = 'astro_categories';
  $handler->handler = 'template';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Template',
    'contexts' => array(),
    'relationships' => array(),
    'module' => 'strongarm',
    'theme' => 'salesforceohana',
    'module_or_theme' => 'theme',
    'template' => 'page--beacon-cat',
    'template_path' => 'templates/system/',
    'hook_theme' => 1,
    'full_page' => 0,
    'name' => 'template',
    'access' => array(
      'logic' => 'and',
    ),
    'theme_function' => 'salesforceohana_template_page__beacon_cat',
    'base' => 'salesforceohana',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['astro_categories'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'post_categories';
  $page->task = 'page';
  $page->admin_title = 'Post Categories';
  $page->admin_description = '';
  $page->path = 'post-cat';
  $page->access = array(
    'logic' => 'and',
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
  );
  $page->menu = array(
    'type' => 'none',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_post_categories__template_929c5ee4-b2ec-4516-9024-5e90b263fc10';
  $handler->task = 'page';
  $handler->subtask = 'post_categories';
  $handler->handler = 'template';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Template',
    'contexts' => array(),
    'relationships' => array(),
    'module' => 'strongarm',
    'theme' => 'salesforceohana',
    'module_or_theme' => 'theme',
    'template' => 'page--post-cat',
    'template_path' => 'templates/system/',
    'hook_theme' => 1,
    'full_page' => 0,
    'name' => '',
    'access' => array(
      'logic' => 'and',
    ),
    'theme_function' => 'salesforceohana_template_page__post_cat',
    'base' => 'salesforceohana',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['post_categories'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'post_industries';
  $page->task = 'page';
  $page->admin_title = 'Post Industries';
  $page->admin_description = '';
  $page->path = 'post-industries';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_post_industries__template_1139e5c8-1d4b-41f6-9c8c-8847661f43d0';
  $handler->task = 'page';
  $handler->subtask = 'post_industries';
  $handler->handler = 'template';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Template',
    'contexts' => array(),
    'relationships' => array(),
    'module' => 'strongarm',
    'theme' => 'salesforceohana',
    'module_or_theme' => 'theme',
    'template' => 'page--post_industries',
    'template_path' => '',
    'hook_theme' => 1,
    'full_page' => 0,
    'name' => '',
    'theme_function' => 'salesforceohana_template_page__post_industries',
    'base' => 'salesforceohana',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['post_industries'] = $page;

  $page = new stdClass();
  $page->disabled = TRUE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'shome';
  $page->task = 'page';
  $page->admin_title = 'shome';
  $page->admin_description = '';
  $page->path = 'shome';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Home',
    'name' => 'menu-signal-menu-top',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_shome__template';
  $handler->task = 'page';
  $handler->subtask = 'shome';
  $handler->handler = 'template';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Template',
    'contexts' => array(),
    'relationships' => array(),
    'module' => 'strongarm',
    'theme' => 'salesforceleadership',
    'module_or_theme' => 'theme',
    'template' => 'page--shome',
    'template_path' => 'templates/system/',
    'hook_theme' => 1,
    'full_page' => 0,
    'name' => 'template',
    'theme_function' => 'salesforceleadership_template_page__shome',
    'base' => 'salesforceleadership',
    'access' => array(
      'plugins' => array(),
    ),
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['shome'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'the_post';
  $page->task = 'page';
  $page->admin_title = 'The Post';
  $page->admin_description = '';
  $page->path = 'post';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Home',
    'name' => 'menu-signal-menu-top',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_the_post__template_1908ca87-918c-48dd-a1ec-a175ac55260c';
  $handler->task = 'page';
  $handler->subtask = 'the_post';
  $handler->handler = 'template';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Template',
    'contexts' => array(),
    'relationships' => array(),
    'module' => 'strongarm',
    'theme' => 'salesforceohana',
    'module_or_theme' => 'theme',
    'template' => 'page--post',
    'template_path' => 'templates/system/',
    'hook_theme' => 1,
    'full_page' => 0,
    'name' => '',
    'theme_function' => 'salesforceohana_template_page__post',
    'base' => 'salesforceohana',
    'access' => array(
      'plugins' => array(),
    ),
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['the_post'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'zone';
  $page->task = 'page';
  $page->admin_title = 'Beacon';
  $page->admin_description = '';
  $page->path = 'beacon';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Home',
    'name' => 'menu-signal-menu-top',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_zone__template_7936b4a1-eb38-4a13-a2e8-9378f0c16413';
  $handler->task = 'page';
  $handler->subtask = 'zone';
  $handler->handler = 'template';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Template',
    'contexts' => array(),
    'relationships' => array(),
    'module' => 'strongarm',
    'theme' => 'salesforceohana',
    'module_or_theme' => 'theme',
    'template' => 'page--beacon',
    'template_path' => 'templates/system/',
    'hook_theme' => 1,
    'full_page' => 0,
    'name' => '',
    'theme_function' => 'salesforceohana_template_page__beacon',
    'base' => 'salesforceohana',
    'access' => array(
      'plugins' => array(),
    ),
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['zone'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'zone_industries';
  $page->task = 'page';
  $page->admin_title = 'Beacon Industries';
  $page->admin_description = '';
  $page->path = 'beacon-industries';
  $page->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 2,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_zone_industries__template';
  $handler->task = 'page';
  $handler->subtask = 'zone_industries';
  $handler->handler = 'template';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Template',
    'contexts' => array(),
    'relationships' => array(),
    'module' => 'strongarm',
    'theme' => 'salesforceohana',
    'module_or_theme' => 'theme',
    'template' => 'page--beacon_industries',
    'template_path' => '',
    'hook_theme' => 1,
    'full_page' => 0,
    'name' => 'template',
    'theme_function' => 'salesforceohana_template_page__beacon_industries',
    'base' => 'salesforceohana',
  );
  $page->default_handlers[$handler->name] = $handler;
  $pages['zone_industries'] = $page;

  return $pages;

}
