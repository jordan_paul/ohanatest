<?php
/**
 * @file
 * the_post_features.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function the_post_features_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_blog_images|node|signal_home|form';
  $field_group->group_name = 'group_blog_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post_content';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'blog post images',
    'weight' => '2',
    'children' => array(
      0 => 'field_blog_featured_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-blog-images field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_blog_images|node|signal_home|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_highlight|node|zone_home|form';
  $field_group->group_name = 'group_highlight';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post_content';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_main_content';
  $field_group->data = array(
    'label' => 'blog highlight',
    'weight' => '25',
    'children' => array(
      0 => 'field_highlight_audio_2',
      1 => 'field_highlight_audio_upload',
      2 => 'field_highlight_link_1',
      3 => 'field_highlight_link_2',
      4 => 'field_highlight_link_3',
      5 => 'field_highlight_video_2',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-highlight field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_highlight|node|zone_home|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_main_content|node|signal_home|form';
  $field_group->group_name = 'group_main_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post_content';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'blog main content',
    'weight' => '3',
    'children' => array(
      0 => 'body',
      1 => 'group_highlight',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-main-content field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_main_content|node|signal_home|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_post_information|node|signal_home|form';
  $field_group->group_name = 'group_post_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'post_content';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'blog post information',
    'weight' => '1',
    'children' => array(
      0 => 'field_category',
      1 => 'field_external_link',
      2 => 'field_first_post',
      3 => 'field_general_tags',
      4 => 'field_industry_cat',
      5 => 'field_internal_chatter_link',
      6 => 'field_post_author',
      7 => 'field_top_section',
      8 => 'field_listing_date',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-post-information field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_post_information|node|signal_home|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('blog highlight');
  t('blog main content');
  t('blog post images');
  t('blog post information');

  return $field_groups;
}
