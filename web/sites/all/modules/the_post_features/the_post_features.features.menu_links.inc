<?php
/**
 * @file
 * the_post_features.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function the_post_features_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-mixed-blog-menu_equality:taxonomy/term/5.
  $menu_links['menu-mixed-blog-menu_equality:taxonomy/term/5'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => 'taxonomy/term/5',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Equality',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_equality:taxonomy/term/5',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: menu-mixed-blog-menu_growth:taxonomy/term/61.
  $menu_links['menu-mixed-blog-menu_growth:taxonomy/term/61'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => 'taxonomy/term/61',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Growth',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_growth:taxonomy/term/61',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-mixed-blog-menu_industries:post-industries.
  $menu_links['menu-mixed-blog-menu_industries:post-industries'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => 'post-industries',
    'router_path' => 'post-industries',
    'link_title' => 'Industries',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_industries:post-industries',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-mixed-blog-menu_the-post:post',
  );
  // Exported menu link: menu-mixed-blog-menu_innovation:post-cat.
  $menu_links['menu-mixed-blog-menu_innovation:post-cat'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => 'post-cat',
    'router_path' => 'post-cat',
    'link_title' => 'Innovation',
    'options' => array(
      'query' => array(
        'cat' => 110,
        'title' => 'Innovation',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_innovation:post-cat',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-mixed-blog-menu_the-post:post',
  );
  // Exported menu link: menu-mixed-blog-menu_innovation:taxonomy/term/3.
  $menu_links['menu-mixed-blog-menu_innovation:taxonomy/term/3'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => 'taxonomy/term/3',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Innovation',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_innovation:taxonomy/term/3',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-mixed-blog-menu_leadership:post-cat.
  $menu_links['menu-mixed-blog-menu_leadership:post-cat'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => 'post-cat',
    'router_path' => 'post-cat',
    'link_title' => 'Leadership',
    'options' => array(
      'query' => array(
        'cat' => 111,
        'title' => 'Leadership',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_leadership:post-cat',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-mixed-blog-menu_the-post:post',
  );
  // Exported menu link: menu-mixed-blog-menu_more:blog-tags.
  $menu_links['menu-mixed-blog-menu_more:blog-tags'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => 'blog-tags',
    'router_path' => 'blog-tags',
    'link_title' => 'More',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_more:blog-tags',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
  );
  // Exported menu link: menu-mixed-blog-menu_newsletter:post.
  $menu_links['menu-mixed-blog-menu_newsletter:post'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => 'post',
    'router_path' => 'post',
    'link_title' => 'Post - Home',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_newsletter:post',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-mixed-blog-menu_the-post:post',
  );
  // Exported menu link: menu-mixed-blog-menu_ohana-blog:blog.
  $menu_links['menu-mixed-blog-menu_ohana-blog:blog'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => 'blog',
    'router_path' => 'blog',
    'link_title' => 'Ohana Blog',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_ohana-blog:blog',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-mixed-blog-menu_our-culture:<front>.
  $menu_links['menu-mixed-blog-menu_our-culture:<front>'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Our Culture',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_our-culture:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-mixed-blog-menu_partners:post-cat.
  $menu_links['menu-mixed-blog-menu_partners:post-cat'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => 'post-cat',
    'router_path' => 'post-cat',
    'link_title' => 'Partners',
    'options' => array(
      'query' => array(
        'cat' => 112,
        'title' => 'Partners',
      ),
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_partners:post-cat',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-mixed-blog-menu_the-post:post',
  );
  // Exported menu link: menu-mixed-blog-menu_the-post:post.
  $menu_links['menu-mixed-blog-menu_the-post:post'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => 'post',
    'router_path' => 'post',
    'link_title' => 'The Post',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_the-post:post',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-mixed-blog-menu_trust:taxonomy/term/1.
  $menu_links['menu-mixed-blog-menu_trust:taxonomy/term/1'] = array(
    'menu_name' => 'menu-mixed-blog-menu',
    'link_path' => 'taxonomy/term/1',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Trust',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-mixed-blog-menu_trust:taxonomy/term/1',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Equality');
  t('Growth');
  t('Industries');
  t('Innovation');
  t('Leadership');
  t('More');
  t('Newsletter');
  t('Ohana Blog');
  t('Our Culture');
  t('Partners');
  t('The Post');
  t('Trust');

  return $menu_links;
}