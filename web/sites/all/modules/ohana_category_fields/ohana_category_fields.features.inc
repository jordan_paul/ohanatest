<?php
/**
 * @file
 * ohana_category_fields.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ohana_category_fields_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}
