<?php

function salesforceohana_form_system_theme_settings_alter(&$form, $form_state) {

  // Blog.
  $form['blog'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blog'),
  );

  $form['blog']['blog_banner_copy'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Banner Title'),
    '#default_value' => theme_get_setting('blog_banner_copy', 'salesforceohana'),
    '#description'   => t("Enter in the copy that you want shown in the banner of all blog pages."),
  );

  $form['blog']['blog_banner_foreground'] = array(
    '#type'          => 'managed_file',
    '#title'         => t('Banner Foreground Image'),
    '#upload_location' => 'public://blog/',
    '#default_value' => theme_get_setting('blog_banner_foreground', 'salesforceohana'),
    '#description'   => t("Upload a custom foreground image to use in the banner. Note that this image is only shown in the desktop view and must be 252 × 167."),
  );

  $form['blog']['blog_banner_background'] = array(
    '#type'          => 'managed_file',
    '#title'         => t('Banner Background Image'),
    '#upload_location' => 'public://blog/',
    '#default_value' => theme_get_setting('blog_banner_background', 'salesforceohana'),
    '#description'   => t("Upload a custom background image to use for the banner. This image should be 2560 × 200."),
  );

  // Hiring.
  $form['hiring'] = array(
    '#type' => 'fieldset',
    '#title' => t('Hiring'),
  );

  $form['hiring']['hiring_hero_copy'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Hero Copy'),
    '#default_value' => theme_get_setting('hiring_hero_copy', 'salesforceohana'),
    '#description'   => t("Enter in the copy that you want in the hero of all hiring pages."),
  );

  $form['hiring']['hiring_hero_image'] = array(
    '#type'          => 'managed_file',
    '#title'         => t('Hero Image'),
    '#upload_location' => 'public://hiring/',
    '#default_value' => theme_get_setting('hiring_hero_image', 'salesforceohana'),
    '#description'   => t("Upload a custom background image to use for the hero."),
  );

  // Onboarding.
  $form['onboarding'] = array(
    '#type' => 'fieldset',
    '#title' => t('Onboarding'),
  );

  $form['onboarding']['onboarding_hero_copy'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Hero Copy'),
    '#default_value' => theme_get_setting('onboarding_hero_copy', 'salesforceohana'),
    '#description'   => t("Enter in the copy that you want in the hero of all onboarding pages."),
  );

  $form['onboarding']['onboarding_hero_image'] = array(
    '#type'          => 'managed_file',
    '#title'         => t('Hero Image'),
    '#upload_location' => 'public://onboarding/',
    '#default_value' => theme_get_setting('onboarding_hero_image', 'salesforceohana'),
    '#description'   => t("Upload a custom background image to use for the hero."),
  );

}

?>
