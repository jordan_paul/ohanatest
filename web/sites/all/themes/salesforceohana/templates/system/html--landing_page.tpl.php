<?php
/**
* Overrides page.tpl.php specifically for Landing Page node type
* Loads some node-specific info so that we can have body classes and custom <head> properties
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>

  <?php
    // Set some variables
    $html_head = '';
    if ( isset($node->field_html_head['und']) ) {
      $html_head = $node->field_html_head['und'][0]['value'];
    }

    $body_classes = '';
    if ( isset($node->field_body_classes['und']) ) {
      $body_classes = $node->field_body_classes['und'][0]['value'];
    }
  ?>

<head>
  <title><?php print $node->title; ?></title>
  <?php print $scripts; ?>
  <?php print $html_head; ?>
</head>

<body <?php print 'class="'.$body_classes.'"'; ?>>

    <?php print $page; ?>

</body>


</html>
