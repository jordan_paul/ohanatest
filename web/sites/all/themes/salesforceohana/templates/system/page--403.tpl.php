<?php
global $base_url;

$returnto = $base_url . check_plain(request_uri());

module_invoke('all','exit');
header('Location: '. '/saml_login?ReturnTo=' . $returnto, TRUE, 302);
?>

<div class="container aloha-login">
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4 image-container col-centered">
            <img src="<?php echo base_path() . drupal_get_path('theme', 'salesforceohana'); ?>/assets/salesforce-logo-large.png" alt="Salesforce Cloud Logo"/>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4 cta-container col-centered">
            <p>Please log in using:</p>
            <a class="btn btn-primary aloha-sso-button" href="<?php echo "/saml_login?ReturnTo=" . $returnto?>">Aloha SSO</a>
        </div>
    </div>

    <?php if(isset($_GET["error"]) && $_GET["error"] === "AuthnFailed") { ?>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-2 error-container col-centered">
            <p>Unfortunately, we could not log you in at this time. Please try again later.</p>
        </div>
    </div>
    <?php } ?>
</div>
