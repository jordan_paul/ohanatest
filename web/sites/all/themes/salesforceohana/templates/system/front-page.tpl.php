<?php
  $image = [];
  $theme_path = base_path() . drupal_get_path('theme', 'salesforceohana') . '/assets/';

  $image['values'] = theme('image', [
    'path' => $theme_path . 'ohanaloha-circle.png',
    'alt'  => 'salesforce ohana',
    'title'  => 'salesforce ohana',
    'attributes' => [ 'class' => 'social-media-icon' ]
  ]);

  $image['ebook'] = theme('image', [
    'path' => $theme_path . 'ebook-banner5.jpg',
    'alt'  => 'salesforce ebook',
    'title'  => 'salesforce ebook',
    'attributes' => [ 'class' => 'social-media-icon' ]
  ]);
?>
<div class="content home">
  <div class="home-introduction">
    <div class="intro-content">
      <h2>Our Culture</h2>
      <p>Many years ago, our CEO Marc Benioff had a vision of a company with a purpose beyond profit ... a company built around the spirit of Ohana. In Hawaiian culture, Ohana represents the idea that families – blood-related, adopted, or intentional – are bound together, and that family members are responsible for one another.</p>

      <p>Today, the #SalesforceOhana is our close-knit ecosystem of employees, customers, partners, and communities. We take care of each other, have fun together, and work collaboratively to make the world a better place.</p>

      <blockquote>
        <p><em><span class="quote-marks">“</span> The business of business is improving the state of the world. <span class="quote-marks">”</span></em></p>
      </blockquote>
      <p><cite>Marc Benioff, Chairman and CEO</cite></p>
    </div>

    <?php print $image['values']; ?>
  </div>

  <hr class="horizontal-rule" />

  <div class="home-values">
    <h2>Our Values</h2>

    <p>Our values are the way we make the vision of Ohana a reality. We are all personally responsible for living and breathing our values: <strong>Trust</strong>, <strong>Customer Success</strong>, <strong>Growth</strong>, <strong>Innovation</strong>, <strong>Giving Back</strong>, <strong>Equality for All</strong>, <strong>Wellbeing</strong>, <strong>Transparency</strong>... and we have <strong>Fun</strong> doing it.</p>

    <p>These values are the essence of our Trusted Culture. They inspire us to do the best work of our lives, and fuel us to deliver unprecedented success for our customers and growth for our company.</p>
  </div>

  <?php include 'front-page-categories.tpl.php'; ?>

  <div class="home-ebook row">
    <div class="col-md-12">
      <h3>Flip through our beautiful Salesforce yearbook</h3>
    </div>
    <div class="col-md-8">
      <p><a href="https://org62.my.salesforce.com/sfc/p/#000000000062/a/0M000000MgX5/fgk6cHR8jTX0owaSBcLIrtZAynggIi0j3xL4BS5Nf84"><?php print $image['ebook']; ?></a></p>
    </div>

    <div class="col-md-8">
    <p>A lot goes on in a year at Salesforce, and FY17 was no exception. Our annual employee yearbook turns the spotlight on our brightest moments and biggest milestones, told firsthand from members of our Ohana. This is the story of Salesforce ... we hope you enjoy it!</p>

    <p><strong><a href="https://org62.my.salesforce.com/sfc/p/#000000000062/a/0M000000MgX5/fgk6cHR8jTX0owaSBcLIrtZAynggIi0j3xL4BS5Nf84" target="_blank">Download the e-book now</a></strong></p>
    </div>

  </div>

  <hr class="horizontal-rule" />

  <div class="home-closing">
    <p>Do you know someone who is bringing our Salesforce values to life?</p>

    <p>Submit his or her story for a chance to be featured on this page and beyond!</p>

    <p><a class="btn" href="https://docs.google.com/a/salesforce.com/forms/d/1E78nr2-m-0ibj3MRqhbP8a4JEfqzceMk0Uab3CegSCA/viewform">Nominate a Colleague</a></p>
  </div>
</div>
