<?php
  // Header Template
  
  
?>
<div class="beacon_theme">
  <nav class="navbar navbar-default signal-top-menu beacon">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-12" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"></a>
      </div>

      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-12">
        <?php 
          $blockheader_post = module_invoke('menu', 'block_view', 'menu-mixed-blog-menu');
          print render($blockheader_post['content']); 
        ?>
      </div>
    </div>
  </nav>

</div>  
