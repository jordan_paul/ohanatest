<?php
/**
 * Overrides page.tpl.php for Hiring and Onboarding Page node type
 */

$logopath = '';

if ($logo) {
  $logopath = $logo;
} else {
  $logopath = drupal_get_path('theme', 'salesforceohana');
  $logopath .= '/assets/salesforce-logo.png';
}

$hero_copy = theme_get_setting('hiring_hero_copy');
if(empty($hero_copy)) {
  $hero_copy = 'Hiring';
}

$hero_url = '/' . drupal_get_path('theme', 'salesforceohana') . '/assets/hiring-onboarding-hero-demo.gif';
$hero_fid = theme_get_setting('hiring_hero_image');
if(!empty($hero_fid)) {
  $hero_file = file_load($hero_fid);
  if($hero_file !== false) {
    if(!empty($hero_file->uri)) {
      $hero_url_raw = file_create_url($hero_file->uri);
      if($hero_url_raw !== false) {
        $hero_url = $hero_url_raw;
      }
    }
  }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>">

<head>
  <title>Hiring :: <?php print $node->title; ?></title>
</head>

<body>

  <header class="header">
    <div class="banner" style="background-image: url('<?php echo $hero_url; ?>');">
      <div class="container">

        <div class="navbar-default">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only"><?php print t('Toggle navigation'); ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <h1><?php print $hero_copy; ?></h1>
      </div>
    </div>

    <?php print render($page['sidebar_first']); ?>
  </header>

  <main id="main-content" class="main-content container">
<?php print $messages; ?>
<?php if (!empty($tabs)): ?>
  <?php print render($tabs); ?>
<?php endif; ?>
<?php print render($breadcrumb); ?>
<?php print render($page['content']); ?>
  </main>

  <footer class="footer">
    <div class="container">
      <img src="<?php print $logopath; ?>" alt="salesforce" />
    </div>
  </footer>

</body>

</html>
