<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup templates
 */
  $logopath2 = '';

  
  if ($logo) {
    $logopath2 = $logo;
  } else {
    $logopath2 = drupal_get_path('theme', 'salesforceohana');
    $logopath2 .= '/assets/salesforce-logo.png';
  }

  $title2 = '';
  if (isset($_GET['title'])) {
    $title2 = $_GET['title'];
  }

  $ids = [];
  if (isset($_GET['id'])) {
    $ids = explode(',', $_GET['id']);
    $finalids = '';
    for ($x = 0; $x <= count($ids); $x++) {
      if ($x == 0) {
        $finalids = $finalids . $ids[$x];
      } else {

        if (isset($ids[$x]) && $ids[$x] != null) {
          $finalids = $finalids . '+' . $ids[$x];
        }
      }
      
    } 
  }
    
  drupal_set_title($title = "The Post");
?>
<?php include 'header_post.tpl.php'; ?>
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  <script>

    
    
  </script>
<div class="main-container container beacon_theme">
  <div class="row">
    

    
    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-4" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section class="col-sm-16">
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>

      <a id="main-content"></a>
      <h2 class="cat-title">Industries<?php if (!empty($title2)): ?>: <?php print $title2;?>
      <?php endif; ?></h2>
      <div class="list-block content-width industry">
        <?php 

          if (isset($_GET['id'])) {
            $array = array($_GET['id']);
            $view = views_get_view('beacon_industry_category');

            $view->set_arguments($array);
            $view->execute();
            print $view->preview();
          } else {
            $array = array('all');
            $view = views_get_view('beacon_industry_category');

            $view->set_arguments($array);
            $view->execute();
            print $view->preview();
          }
            
        ?>
      </div>
      <aside class="col-sm-5 right-side" role="complementary">
        <div class="industries-menu">
          <?php 
            $block_menu = module_invoke('menu', 'block_view', 'menu-beacon-industry-menu');
            print render($block_menu['content']); 
          ?>
        </div>
        <div class="region region-sidebar-second">
          <!--?php print render($page['sidebar_second']); ?-->
          <?php 
            $block_search = module_invoke('search', 'block_view', 'search');
            print render($block_search['content']);  
          ?> 

          
          <h2 class="block-title">Popular Tags</h2>

          <?php 
            $block_popular = module_invoke('menu', 'block_view', 'menu-beacon-menu-top');
            print render($block_popular['content']); 
          ?>

          <?php 
              $block_topposts = module_invoke('views', 'block_view', 'beacon_top_posts-block');
          ?>
          <h2 class="block-title"><?php print $block_topposts['subject']; ?></h2>
          <?php
            print render($block_topposts['content']); 
          ?>

          <br />
        </div>
          
      </aside>  
    </section>

  </div>
</div>

<?php if (!empty($page['footer'])): ?>
  <footer class="footer <?php print $container_class; ?>">
    <img src="<?php print $logopath2 ?>" />
    <?php 
        $block_footer = module_invoke('menu', 'block_view', 'menu-mixed-blog-menu');
        print render($block_footer['content']); 
      ?>
  </footer>
<?php endif; ?>


