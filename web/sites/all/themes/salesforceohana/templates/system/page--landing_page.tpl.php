<?php
/**
 * Overrides page.tpl.php for Landing Page node type
 * Only outputs the custom body field from landing page node
 */

 $html_body = $node->field_html_body['und'][0];

?>

<?php print $html_body['value']; ?>
