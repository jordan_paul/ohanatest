<?php
$banner_copy = theme_get_setting('blog_banner_copy');
if(empty($banner_copy)) {
  $banner_copy = '#SalesforceOhana';
}

$banner_foreground_url = '/' . drupal_get_path('theme', 'salesforceohana') . '/assets/blog-banner-foreground.png';
$banner_foreground_fid = theme_get_setting('blog_banner_foreground');
if(!empty($banner_foreground_fid)) {
  $banner_foreground_file = file_load($banner_foreground_fid);
  if($banner_foreground_file !== false) {
    if(!empty($banner_foreground_file->uri)) {
      $banner_foreground_url_raw = file_create_url($banner_foreground_file->uri);
      if($banner_foreground_url_raw !== false) {
        $banner_foreground_url = $banner_foreground_url_raw;
      }
    }
  }
}

$banner_background_url = '/' . drupal_get_path('theme', 'salesforceohana') . '/assets/blog-banner-background.jpg';
$banner_background_fid = theme_get_setting('blog_banner_background');
if(!empty($banner_background_fid)) {
  $banner_background_file = file_load($banner_background_fid);
  if($banner_background_file !== false) {
    if(!empty($banner_background_file->uri)) {
      $banner_background_url_raw = file_create_url($banner_background_file->uri);
      if($banner_background_url_raw !== false) {
        $banner_background_url = $banner_background_url_raw;
      }
    }
  }
}
?>

<header id="navbar" role="banner" class="<?php print $navbar_classes; ?>" style="background-image: url('<?php echo $banner_background_url; ?>');">
  <div class="navbar-header">

    <?php if (!empty($primary_nav) || !empty($secondary_nav)): ?>
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?php print t('Toggle navigation'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    <?php endif; ?>

    <div class="navbar-banner-copy">
      <h1><?php echo $banner_copy; ?></h1>
      <div class="foreground-img"><img src="<?php echo $banner_foreground_url; ?>" alt="" /></div>
    </div>

  </div>
</header>

<?php if (!empty($primary_nav) || !empty($secondary_nav)): ?>
  <div class="navigation-container navbar-collapse collapse">
    <nav role="navigation">
      <?php if (!empty($primary_nav)): ?>
        <?php print render($primary_nav); ?>
      <?php endif; ?>
      <?php if (!empty($secondary_nav)): ?>
        <?php print render($secondary_nav); ?>
      <?php endif; ?>
    </nav>
  </div>
<?php endif; ?>
