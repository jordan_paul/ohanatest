<?php
  $vocab = taxonomy_vocabulary_machine_name_load('ohana_categories');
  $terms = taxonomy_get_tree($vocab->vid, 0);
  $vocab_tids = [];
  $values = [];

  foreach ($terms as $term) {
    $vocab_tids[] = $term->tid;
  }

  $values_mapped_by_tid = taxonomy_term_load_multiple($vocab_tids);

  foreach ($values_mapped_by_tid as $vtid) {
    $values[$vtid->name] = $vtid;
  }

  // dpm($values);

  function image_maker($category) {
    $image_value = $category->field_category_featured_image;

    if (!count($image_value)) {
      return false;
    } else {
      $image_value = $image_value['und'][0];
      $alt = $image_value['alt'];
      $title = $image_value['title'];
      $uri = $image_value['uri'];

      $image = '<img ';
      if (count($alt)) {
        $image .= 'alt="' . $alt . '" ';
      }
      if (count($title)) {
        $image .= 'title="' . $title . '" ';
      }
      $image .= 'src="' . file_create_url($uri) . '"';
      $image .= ' />';

      return $image;
    }
  }
?>
<div class="home-category-tabs">
  <ul class="tab-legend">
    <li class="trust active">Trust</li>
    <li class="cust-success">Customer Success</li>
    <li class="growth">Growth</li>
    <li class="innovation">Innovation</li>
    <li class="giving-back">Giving Back</li>
    <li class="equality">Equality</li>
    <li class="wellbeing">Wellbeing</li>
    <li class="transparency">Transparency</li>
    <li class="fun">Fun</li>
  </ul>

  <div class="tab-content-container">

    <?php
      $value_map = [
        [ 'subject' => 'trust', 'name' => 'Trust' ],
        [ 'subject' => 'cust-success', 'name' => 'Customer Success' ],
        [ 'subject' => 'growth', 'name' => 'Growth' ],
        [ 'subject' => 'innovation', 'name' => 'Innovation' ],
        [ 'subject' => 'giving-back', 'name' => 'Giving Back' ],
        [ 'subject' => 'equality', 'name' => 'Equality For All' ],
        [ 'subject' => 'wellbeing', 'name' => 'Wellbeing' ],
        [ 'subject' => 'transparency', 'name' => 'Transparency' ],
        [ 'subject' => 'fun', 'name' => 'Fun' ]
      ];

      foreach ($value_map as $key=>$v) : ?>
    <div class="tab-content<?php if ($key) { echo ' tab-hidden'; } ?>" data-subject="<?php echo $v['subject']; ?>">
      <h3><?php print $values[$v['name']]->name; ?></h3>
      <div class="content-body">
        <?php print render($values[$v['name']]->description); ?>

        <?php if (isset($values[$v['name']]->field_category_home_cta) && count($values[$v['name']]->field_category_home_cta)) : ?>
        <p><?php print l($values[$v['name']]->field_category_home_cta['und'][0]['safe_value'], 'taxonomy/term/' . $values[$v['name']]->tid, ['attributes' => ['class' => 'btn']]); ?></p>
        <?php endif ?>
      </div>

      <?php
        $value_image = image_maker($values[$v['name']]);
        if ($value_image) {
          print $value_image;
        }
      ?>
    </div>
    <?php endforeach; ?>
  </div>
</div>
