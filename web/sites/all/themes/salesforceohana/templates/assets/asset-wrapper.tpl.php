<?php
/**
 * @file
 * Asset wrapper template.
 */
// dpm(get_defined_vars());
// dpm($content);
// dpm($attributes);

// Let's help with the alignment classes
// the embeds do not get the right, left, center align classes
if ($element['#entity_view_mode']['bundle'] === 'embed') {
  $asset_description_array = explode(',', $variables['element']['#entity']->asset_source_string);
  $asset_description = [];

  foreach ($asset_description_array as $da) {
    $new_info = explode(':', $da);
    // HACKJKSDJHF
    $asset_description[str_replace('"', '', $new_info[0])] = str_replace('"', '', $new_info[1]);
  }

  $align_class = '';
  if (isset($asset_description['align'])) {
    $align_class = 'asset-align-' . $asset_description['align'];
    $attributes_array['class'][] = $align_class;

    $attributes = drupal_attributes($attributes_array);
  }
}
?>
<div <?php print $attributes; ?>><?php print $content; ?></div>
