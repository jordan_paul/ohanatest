<?php

/**
 * @file
 * Default theme implementation to display a term.
 *
 * Available variables:
 * - $name: (deprecated) The unsanitized name of the term. Use $term_name
 *   instead.
 * - $content: An array of items for the content of the term (fields and
 *   description). Use render($content) to print them all, or print a subset
 *   such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $term_url: Direct URL of the current term.
 * - $term_name: Name of the current term.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - taxonomy-term: The current template type, i.e., "theming hook".
 *   - vocabulary-[vocabulary-name]: The vocabulary to which the term belongs to.
 *     For example, if the term is a "Tag" it would result in "vocabulary-tag".
 *
 * Other variables:
 * - $term: Full term object. Contains data that may not be safe.
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $page: Flag for the full page state.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the term. Increments each time it's output.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_taxonomy_term()
 * @see template_process()
 *
 * @ingroup themeable
 */

  // dpm($content);

  $has = [];
  $has['description'] = isset($content['description']);
  $has['intro'] = isset($content['field_category_intro_headline']);
  $has['cta_description'] = isset($content['field_category_cta_description']);
  $has['examples'] = isset($content['field_category_example']);
  $has['cta_link'] = isset($content['field_category_cta_link']);
  $has['additional_info'] = isset($content['field_category_addtional_info']);

  if ($has['description']) {
    $description = $content['description']['#markup'];
  }

  if ($has['intro']) {
    $intro_headline = $content['field_category_intro_headline']['#items'][0]['safe_value'];
  }

  if ($has['cta_description']) {
    $cta_description = $content['field_category_cta_description']['#items'][0]['value'];
  }

  if ($has['additional_info']) {
    $additional_info = $content['field_category_addtional_info']['#items'][0]['value'];
  }

  $classes .= ' term-' . str_replace(' ', '-', strtolower($name));
?>

<div id="taxonomy-term-<?php print $term->tid; ?>" class="<?php print $classes; ?>">

  <div class="content">

    <div class="taxonomy-introduction">
      <?php print render($content['field_category_featured_image']); ?>

      <div class="intro-content">
        <h2><?php print $name; ?></h2>
        <?php if ($has['description']) {
          print $description;
        } ?>
      </div>
    </div>

    <!-- 
    <?php if ($has['intro']) :?>
    <hr class="horizontal-rule" />
    <?php endif; ?>

    <div class="taxonomy-examples">
      <?php if ($has['intro']) :?>
        <h3><?php print $intro_headline; ?></h3>
      <?php endif; ?>

      <?php if ($has['examples']) {
        foreach($content['field_category_example'] as $key=>$field_collection) {
          if(is_numeric($key) && !empty($field_collection['entity']['field_collection_item'])) {
            $example = current($field_collection['entity']['field_collection_item']);
      ?>
      <div class="content-example">
        <div class="content-example-image">
          <?php print render($example['field_category_example_image']); ?>
        </div>

        <div class="content-example-content">
          <?php print render($example['field_category_example_content']); ?>
        </div>
      </div>
      <?php // close the weird nested tags
          }; // endif this was a good key to go through
        }; // endforeach
      }; // endif we have examples ?>
    </div>

    <?php if ($has['cta_description']) : ?>
    <hr class="horizontal-rule" />
    <?php endif ?>

    <div class="taxonomy-cta">
      <?php if ($has['cta_description']) : ?>
      <div class="cta-body">
        <?php print $cta_description; ?>
      </div>
      <?php endif ?>


      <?php if ($has['cta_link']) : ?>
      <div class="cta-link">
        <?php print render($content['field_category_cta_link']); ?>
      </div>
      <?php endif ?>
    </div>
  -->

    <?php if ($has['additional_info']) : ?>
      <hr class="horizontal-rule" />
      <?php print $additional_info; ?>
    <?php endif ?>

  </div>
</div>
