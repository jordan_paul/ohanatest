<?php
  // dpm($content);
  // dpm($node);

  $has = [];
  $has['tags'] = isset($content['field_blog_tagging']);
  $has['featured_img'] = isset($content['field_blog_featured_image']);
?>
<div class="node-blogpost--teaser">
  <div class="blog-information">
    <h2 class="blog-title">

      <?php 
        $limit = 50;
        //if (strlen($title) > $limit) {$title= substr($title, 0, $limit) . '...';}
        echo l($title, 'node/' . $node->nid, ['html' => true]); 
      ?>

      
    </h2>
    <p class="date-tags"><?php print date('F j, Y', $created); ?></p>
    <div class="ohana-tags">
      | <?php
        $tags = $content['field_category'];
        $tag_limit = 1;
        for ($i = 0; $i <= $tag_limit - 1; $i++) { 
          if ($tags[$i] != null) : ?>
          <span class="label-tags">
            <?php print render($tags[$i]); ?>
          </span>
          <?php endif; ?>
      <?php } ?>

    </div>
    <?php print render($content['body']); ?>
  </div>

  <div class="blog-banner">
    <div class="blog-img">
      <p class="date-tags2"><?php print date('F j, Y', $created); ?></p>
      <?php 
          print render($content['field_blog_featured_image']);
      ?>
    </div>
      <div class="pull-right beacon-tags">
        <?php
          $tags = $content['field_category'];
          $tag_limit = 1;
          for ($i = 0; $i <= $tag_limit - 1; $i++) { 
            if ($tags[$i] != null) : ?>
            <span class="label-tags">
              <?php print render($tags[$i]); ?>
            </span>
            <?php endif; ?>
        <?php } ?>

      </div>
  </div>
</div>
