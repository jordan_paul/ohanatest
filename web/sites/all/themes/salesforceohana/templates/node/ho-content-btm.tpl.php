<?php
  if ( isset($content['field_content_btm_list']) ) :
?>

<div class="content-item content-btm">

  <?php
    foreach($content['field_content_btm_list'] as $key=>$field_collection) :
      if(is_numeric($key) && !empty($field_collection['entity']['field_collection_item'])) :
        $copy = current($field_collection['entity']['field_collection_item']);
  ?>

  <div class="content-item <?php print render($copy['field_content_btm_type']); ?>">
<?php print render($copy['field_content_btm_copy']); ?>
  </div>

  <?php
      endif;
    endforeach;
  ?>

</div>

<?php
  endif;
?>
