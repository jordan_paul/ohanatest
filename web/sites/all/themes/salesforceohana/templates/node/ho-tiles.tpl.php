<?php
  if ( isset($content['field_tiles_list']) ) :
?>

<div class="content-item tiles">

<div class="row">
  <?php if ( isset($content['field_tiles_headline']) ) : ?>
  <div class="col-xs-12">
    <h2><?php print render($content['field_tiles_headline']); ?></h2>
  </div>
  <?php endif; ?>
</div>

<div class="tile-list">


  <?php
    foreach($content['field_tiles_list'] as $key=>$field_collection) :
      if(is_numeric($key) && !empty($field_collection['entity']['field_collection_item'])) :
        $tile = current($field_collection['entity']['field_collection_item']);
  ?>

  <div class="tile">
    <a href="<?php print render($tile['field_tile_link']); ?>"
      <?php if ( ( isset($tile['field_new_window']) ) && ( $tile['field_new_window']['#items'][0]['value'] === '1') ) : ?>
        target="_blank"
      <?php endif; ?>
      >
      <div class="wrapper">
        <div class="front">
          <h3><?php print render($tile['field_tile_title']); ?></h3>
        </div>
        <div class="back">
          <h3><?php print render($tile['field_tile_title']); ?></h3>
          <p><?php print render($tile['field_tile_copy']); ?></p>
        </div>
      </div>
    </a>
  </div>

  <?php
      endif;
    endforeach;
  ?>

</div>

</div>

<?php
  endif;
?>
