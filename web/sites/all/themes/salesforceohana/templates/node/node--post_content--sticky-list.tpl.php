<?php
  // dpm($content);
  // dpm($node);
?>
<div class="node-blogpost--sticky-list">
  <!--div class="blog-banner sidebar-banner">
    <?php print render($content['field_blog_featured_image']); ?>
  </div-->

  <div class="blog-information">
    <h3 class="blog-title">
      <?php echo l($title, 'node/' . $node->nid, ['html' => true]); ?>
    </h3>

    <div class="blog-categories">
      <div class="beacon-sidebar-tags">#
      <?php
        $tags = $content['field_category'];
        $tag_limit = 1;
        for ($i = 0; $i <= $tag_limit - 1; $i++) { 
          if ($tags[$i] != null) : 
             print render($content['field_category']);
          endif;
      } ?>
      </div>
    </div>
  </div>
</div>
