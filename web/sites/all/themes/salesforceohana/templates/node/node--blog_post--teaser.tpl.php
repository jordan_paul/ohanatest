<?php
  // dpm($content);
  // dpm($node);

  $has = [];
  $has['tags'] = isset($content['field_blog_tagging']);
  $has['square_img'] = isset($content['field_blog_square_image']);
?>
<div class="node-blogpost--teaser">
  <div class="blog-information">
    <h2 class="blog-title">
      <?php echo l($title, 'node/' . $node->nid, ['html' => true]); ?>
    </h2>

    <p>
      <?php print date('F j, Y', $created); ?>
      |
      <span class="blog-categories">
        <?php print render($content['field_blog_category']); ?>
      </span>

      <?php if ($has['tags']) : ?>
      |
      <span class="blog-tags">
        <?php
          $tags = $content['field_blog_tagging'];
          $tag_limit = 3;

          if (!count($tags['#items']) > $tag_limit) {
            print render($tags);
          } else {
            for ($i = 0; $i <= $tag_limit - 1; $i++) {
              if ($i !== 0) { print ' '; }
              print render($tags[$i]);
            }
          }
        ?>
      </span>
      <?php endif; ?>
    </p>

    <?php print render($content['body']); ?>
  </div>

  <div class="blog-banner">
    <?php if ($has['square_img']) {
        print render($content['field_blog_square_image']);
      } else {
        print render($content['field_blog_featured_image']);
      }
    ?>
  </div>
</div>
