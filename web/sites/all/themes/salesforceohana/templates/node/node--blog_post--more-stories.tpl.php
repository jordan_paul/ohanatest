<?php
  // dpm($content);
  // dpm($node);
  $has = [];
  $has['square_img'] = isset($content['field_blog_square_image']);
?>
<div class="node-blogpost--more-stories">
  <div class="blog-banner">
    <?php if ($has['square_img']) {
        print render($content['field_blog_square_image']);
      } else {
        print render($content['field_blog_featured_image']);
      }
    ?>
  </div>

  <div class="blog-information">
    <h3 class="blog-title">
      <?php echo l($title, 'node/' . $node->nid, ['html' => true]); ?>
    </h3>
  </div>
</div>
