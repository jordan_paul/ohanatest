<?php
  if ( isset($content['field_content_top_list']) ) :
?>

<div class="content-item content-top">

  <?php
    foreach($content['field_content_top_list'] as $key=>$field_collection) :
      if(is_numeric($key) && !empty($field_collection['entity']['field_collection_item'])) :
        $copy = current($field_collection['entity']['field_collection_item']);
  ?>

  <div class="content-item <?php print render($copy['field_content_top_type']); ?>">
<?php print render($copy['field_content_top_copy']); ?>
  </div>

  <?php
      endif;
    endforeach;
  ?>

</div>

<?php
  endif;
?>
