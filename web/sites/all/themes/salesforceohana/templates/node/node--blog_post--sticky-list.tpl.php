<?php
  // dpm($content);
  // dpm($node);
?>
<div class="node-blogpost--sticky-list">
  <div class="blog-banner">
    <?php print render($content['field_blog_featured_image']); ?>
  </div>

  <div class="blog-information">
    <h3 class="blog-title">
      <?php echo l($title, 'node/' . $node->nid, ['html' => true]); ?>
    </h3>

    <p class="blog-categories">
      #<?php print render($content['field_blog_category']); ?>
    </p>
  </div>
</div>
