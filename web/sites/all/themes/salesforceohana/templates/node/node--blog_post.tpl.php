<?php
  // dpm($content);
  // dpm($node);

  $blog_type = field_get_items('node', $node, 'field_blog_type')[0]['value'];

  $has = [];

  $has['banner_description'] = !empty( $content['field_blog_featured_image'][0]['#item']['title'] );

  $has['extra_links'] = false;
  $has['external_link'] = false;
  if (isset($content['field_blog_external_link'])) {
    $has['extra_links'] = true;
    $has['external_link'] = true;
  }

  $has['chatter_link'] = false;
  if (isset($content['field_blog_internal_chatter_link'])) {
    $has['extra_links'] = true;
    $has['chatter_link'] = true;
  }

  $has['ending'] = isset($content['field_blog_ending_remarks']);
  $has['ending_cta'] = isset($content['field_blog_ending_remarks_cta']);

  $has['image_grid'] = isset($content['field_blog_image_grid']) && $blog_type === 'standard-with-images';
  $has['rollup'] = isset($content['field_category_example']) && $blog_type === 'roundup';
  $has['video'] = isset($content['field_blog_video_embed']) && $blog_type === 'video';

  $has['tags'] = isset($content['field_blog_tagging']);
?>

<div class="mobile-back-container">
    <a class="btn btn-primary" href="/blog"><i class="glyphicon glyphicon-chevron-left"></i>Back to Blog</a>
</div>

<h2 class="blog-title"><?php print $title; ?></h2>

<div class="blog-banner <?php if ($has['video']) { echo 'with-video'; } ?>">
  <?php if ($has['video']) {
    print $content['field_blog_video_embed']['#items'][0]['value'];
  } else {
    print render($content['field_blog_featured_image']);
  } ?>

  <?php if ($has['banner_description']) : ?>
  <p class="description"><em><?php print t($content['field_blog_featured_image'][0]['#item']['title']); ?></em></p>
  <?php endif; ?>
</div>

<div class="blog-information">
  <p>
    by <?php print render($content['field_blog_author']); ?>
    | <?php print date('F j, Y', $created); ?>
  </p>

  <?php if ($has['extra_links']) : ?>
  <p>
    <?php if($has['chatter_link']) :?>
    Share on <?php print render($content['field_blog_internal_chatter_link']); ?>
    <?php endif; ?>

    <?php if($has['external_link']) :?>
    <?php if ($has['chatter_link']) { print ' | '; } ?>
    <?php print render($content['field_blog_external_link']); ?>
    <?php endif; ?>
  </p>
  <?php endif; ?>
</div>

<div class="blog-content">
  <?php print render($content['body']); ?>

  <?php if ($has['rollup']) : ?>
  <div class="blog-rollup content-examples">
    <?php foreach($content['field_category_example'] as $key=>$field_collection) {
      if(is_numeric($key) && !empty($field_collection['entity']['field_collection_item'])) {
        $example = current($field_collection['entity']['field_collection_item']);
    ?>
    <div class="content-example">
      <div class="content-example-image">
        <?php print render($example['field_category_example_image']); ?>
      </div>

      <div class="content-example-content">
        <?php print render($example['field_category_example_content']); ?>
      </div>
    </div>
    <?php // close the weird nested tags
        }; // endif this was a good key to go through
      }; // endforeach
    ?>
  </div>
  <?php endif; // endif we have examples ?>
  
  <?php print render($content['field_body_paragraph_bottom']); ?>


</div>

<?php if ($has['image_grid']) : ?>
<div class="blog-image-grid">
  <?php
    print render($content['field_blog_image_grid']);
  ?>
</div>
<?php endif; ?>

<?php if ($has['tags']) : ?>
<p class="blog-tags">tagged as: <?php print render($content['field_blog_tagging']); ?></p>
<?php endif; ?>

<?php if ($has['ending']) : ?>
<div class="blog-addendum">
  <div class="callout">
    <?php print render($content['field_blog_ending_remarks']); ?>
  </div>

  <?php if ($has['ending_cta']) : ?>
  <div class="cta">
    <?php print render($content['field_blog_ending_remarks_cta']); ?>
  </div>
  <?php endif; ?>
</div>
<?php endif; ?>

<?php
    // Related Content
    if ($node) {

        $tag = $node->field_blog_category;
        $cat = $node->field_blog_tagging;

        // Set the argument for the view
        if (count($cat) && count($tag)) {
            $viewarg = $tag['und'][0]['tid'].'+'.$cat['und'][0]['tid'];
        } else if (count($cat) && !count($tag)) {
            $viewarg = $cat['und'][0]['tid'];
        } else {
            $viewarg = $tag['und'][0]['tid'];
        }

        // Display the Related Posts view with the arguments passed in
        print '<h3>Related Blog Posts</h3>';
        $view = views_get_view('stick_posts_list');

        // Pass in the categories (1) and exclude the current node (2)
        $view->set_arguments(array($viewarg,$node->nid));

        print $view->preview('block_1');
            $view->destroy();
    }
?>
