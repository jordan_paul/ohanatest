<?php

/**
 * Overrides search-result.tpl.php with custom author and no list-styles
 */
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <h3 class="title"<?php print $title_attributes; ?>>
    <a href="<?php print $url; ?>"><?php print $title; ?></a>
  </h3>
  <?php print render($title_suffix); ?>
  <div class="search-snippet-info">
    <?php if ($snippet): ?>
      <p class="search-snippet"<?php print $content_attributes; ?>><?php print $snippet; ?></p>
    <?php endif; ?>

    <?php 
    $author = $result['node']->field_blog_author['und'][0];
    $author_name = $author['title'];
    $author_link = $author['url'];
    $author_target = $author['attributes']['target'];
    ?>

    <p>
      by 
      <?php print '<a href="'.$author_link.'" target="'.$author_target.'">'.$author['title'].'</a>' ?>
      |
      <?php print date('F j, Y', $result['node']->created); ?>
    </p>
  </div>
  <hr />
</div>
