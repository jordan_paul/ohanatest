<?php

/**
 * Overrides search-results.tpl.php with no list styles
 */
?>
<?php if ($search_results): ?>
  <h2><?php print t('Search results');?></h2>
  <?php print $search_results; ?>
  <?php print $pager; ?>
<?php else : ?>
  <h2><?php print t('Your search yielded no results');?></h2>
  <?php print search_help('search#noresults', drupal_help_arg()); ?>
<?php endif; ?>
