<?php
/**
 * @file
 * The primary PHP file for this theme.
 */

function salesforceohana_preprocess_page(&$vars)
{
    $status = drupal_get_http_header("status");
    $node = menu_get_object();

    if ($status == '403 Forbidden') {
        $vars['theme_hook_suggestions'][] = 'page__403';
    }

    // Allow for node type specific page.tpl.php (page--NODE_TYPE.tpl.php)
    if ($node && $node->nid) {
        $vars['theme_hook_suggestions'][0] = 'page__' . $node->type;
    }

    
    // Custom routing for post_category and post_industries taxonomies
    if (arg(0) == 'taxonomy') {
        $vocab = taxonomy_vocabulary_machine_name_load('post_categories');
        $vid = (int)$vocab->vid;
        $vocab2 = taxonomy_vocabulary_machine_name_load('post_industries');
        $vid2 = (int)$vocab2->vid;

        $term = taxonomy_term_load(arg(2));
        $machine = (int)$term->vid;
        if ($machine == $vid || $machine == $vid2) {
            $vars['theme_hook_suggestions'][] = 'page__taxonomy_post';
        }
      }
    
    // customise for a 16 col grid
    if (!empty($vars['page']['sidebar_first']) && !empty($vars['page']['sidebar_second'])) {
        $vars['content_column_class'] = ' class="col-sm-7"';
    } elseif (!empty($vars['page']['sidebar_first']) || !empty($vars['page']['sidebar_second'])) {
        $vars['content_column_class'] = ' class="col-sm-11"';
    } else {
        $vars['content_column_class'] = ' class="col-sm-16"';
    }


    $alias_parts = explode('/', drupal_get_path_alias());

    if (count($alias_parts) && $alias_parts[0] == 'mobile-auth') {
        $vars['theme_hook_suggestions'][] = 'page__ohana_mobile_auth';

    } else if (count($alias_parts) && $alias_parts[0] == 'oauth') {
        $vars['theme_hook_suggestions'][] = 'page__ohana_mobile_auth';

    }

    // $alias_parts = explode('/', drupal_get_path_alias());


    // if (count($alias_parts) && $alias_parts[0] == 'oauth') {
    //     print '<pre>';
    //     print_r($vars['theme_hook_suggestions']);
    //     print '</pre>';
    //     $vars['theme_hook_suggestions'][] = 'page__ohana_oauth';

    // }
}

function salesforceohana_js_alter(&$js) {

    // Load node for node type check (different behavior in js_alter hook)
    // http://stackoverflow.com/questions/10138660/how-to-remove-css-and-js-from-a-certain-content-type-in-drupal-7
    if(arg(0) == 'node' && arg(1) != 0) {
        $node = node_load(arg(1));
        if (
          $node &&
          (
            ( $node->type == 'landing_page' ) ||
            ( $node->type == 'hiring' ) ||
            ( $node->type == 'onboarding' )
          )
        ) {
            // Unset javascript that you don't need for landing pages
            unset($js['profiles/SalesforceOhana/modules/contrib/environment_indicator/tinycon.min.js']);
            unset($js['profiles/SalesforceOhana/modules/contrib/environment_indicator/environment_indicator.js']);
            unset($js['profiles/SalesforceOhana/modules/contrib/environment_indicator/color.js']);
            unset($js['profiles/SalesforceOhana/modules/contrib/devel/devel_krumo_path.js']);
            unset($js['profiles/SalesforceOhana/modules/contrib/coffee/js/coffee.js']);
            unset($js['profiles/SalesforceOhana/modules/contrib/responsive_menus/styles/sidr/js/responsive_menus_sidr.js']);
            unset($js['profiles/SalesforceOhana/libraries/sidr/jquery.sidr.min.js']);
            unset($js['sites/all/themes/salesforceohana/js/tab-content.js']);
        }
    }
}

function salesforceohana_css_alter(&$css) {
  $removeHiringOnboardingCSS = true;

  if(arg(0) == 'node' && arg(1) != 0) {
      $node = node_load(arg(1));
      if (
        $node &&
        (
          ( $node->type == 'hiring' ) ||
          ( $node->type == 'onboarding' )
        )
      ) {
        $oldCSS = path_to_theme() . '/css/style.css';
        unset($css[$oldCSS]);

        $removeHiringOnboardingCSS = false;
      }
  }

  if ($removeHiringOnboardingCSS) {
    $oldCSS = path_to_theme() . '/css/hiring-onboarding.css';
    unset($css[$oldCSS]);
  }
}

function salesforceohana_preprocess_html(&$vars)
{
    $node = menu_get_object();

    // Allow for node type specific html.tpl.php (html--NODE_TYPE.tpl.php)
    if ($node && $node->nid) {
        $vars['theme_hook_suggestions'][0] = 'html__' . $node->type;

        if($node->type == 'landing_page') {
            // Load the node (again) so we can use the proper fields
            $vars['node'] = $node;
        }
    }
}
