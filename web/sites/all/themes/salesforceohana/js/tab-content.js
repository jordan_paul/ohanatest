(function() {
  'use strict';
  var controls, container;

  jQuery(document).ready(initTabContent);

  ///////

  function initTabContent() {
    controls = jQuery('.tab-legend');

    if (controls.length) {
      controls.find('li').click(changeActiveTab);

      container = jQuery('.tab-content-container');
    } else {
      return false;
    }
  }

  function changeActiveTab() {
    var target;

    controls.find('li').removeClass('active');
    target = jQuery(this).attr('class');
    jQuery(this).addClass('active');

    container.find('.tab-content').addClass('tab-hidden');
    container.find('.tab-content[data-subject=' + target + ']').removeClass('tab-hidden');
  }
})();
