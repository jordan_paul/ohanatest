FROM heroku/php

RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
RUN composer global require drush/drush:7.*
RUN echo "export PATH='$HOME/.composer/vendor/bin:$PATH'" >> /root/.bashrc
